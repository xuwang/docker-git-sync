include envs
export

help: ## this info
	@# adapted from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo '_________________'
	@echo '| Make targets: |'
	@echo '-----------------'
	@cat Makefile | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build gcsfuse image
	docker-compose build

up: ## bring system up, see docker-compose.yml
	@if ! docker images alpine-git | grep -q alpine-git ; \
	then \
		make build; \
	fi
	docker-compose up -d --force-recreate --remove-orphans

down: ## bring system dev-down, see docker-compose.yml
	docker-compose down --remove-orphans --volumes

restart: ## restart test system
	docker-compose restart

follow: ## docker-compose logs -f
	docker-compose logs -f

prune: ## prune local docker containers and images
	docker container prune -f
	docker image prune -f
	docker volume prune -f

gen-key: ## generate git deploy key
	@mkdir -p keys
	ssh-keygen -t rsa -C "docker-examples" -b 4096 -P "" -f keys/docker-examples

.PHONY: build prune push pull help up down restart follow gen-key