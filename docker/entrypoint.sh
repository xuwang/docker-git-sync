#!/bin/sh

# just in case ...
chown -R ${UID}:${UID} ${DATA_DIR}

# run with given user
exec sudo -u "#${UID}" gcsfuse  \
  --foreground \
  --key-file=${GOOGLE_APPLICATION_CREDENTIALS} ${BUCKET_NAME} ${DATA_DIR}