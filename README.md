# Docker Example - Periodically sync data from a git repo

## Prerequisites

#### Set the sync parameters

Edit env file, and set:

```
# sync git repo
REPO=<content git repo git+ssh url>
# git repo deploy key
DEPLOY_KEY=<git repo deploy key>
# pull interval in seconds
INTERVAL=3
```

## Build the alpine-git docker image

```
$ make build
```

## See it works

```
$ make up follow
...
producer_1  | Initialized empty Git repository in /data/.git/
producer_1  | Producer: update /data from git@code.stanford.edu:xuwang/docker-git-sync.git
consumer_1  | Consumer: commit 25a78a835e6dfc6e963650b9d3ed577a028e1896 Author: Xu Wang <xuwang@gmail.com> Date: Sun Apr 30 19:35:00 2017 -0700 docker example: sync git repo in container
...
```

## Shut it down

```
$ make down
```
