#!/bin/sh

# content git repo
REPO=${REPO:-"git@code.stanford.edu:xuwang/docker-git-sync.git"}
# destination dir
DATA=${DATA:-/data}
# git repo deploy key
DEPLOY_KEY=${DEPLOY_KEY:-/keys/docker-examples}
# pull interval in seconds
INTERVAL=${INTERVAL:-60}
git config --global user.name ${GIT_USER:-Docker Examples}
git config --global user.email ${GIT_USER:-docker-examples@exmaple.com}

# git ssh cmd
# why options, see https://www.joedog.org/2012/07/13/ssh-disable-known_hosts-prompt
export GIT_SSH_COMMAND="ssh -q -i $DEPLOY_KEY \
		-o 'StrictHostKeyChecking=no' -o 'UserKnownHostsFile=/dev/null'"

# Sync content from git repo to /content on given time interval
if [ ! -d "${DATA}" ]; then mkdir -p ${DATA}; fi
cd $DATA
while true
do
	if [ ! -d ".git" ]
	then
		git init
		git remote add origin ${REPO}
	fi
	echo Producer: update $DATA from ${REPO}
	git fetch origin master --depth=1 -f -q
	git reset --hard origin/master -q
	git log | head > git-log.txt
	sleep ${INTERVAL}
done
